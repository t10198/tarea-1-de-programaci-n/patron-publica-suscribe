##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: main.py
# Capitulo: Estilo Publica-Suscribe
# Autor(es): Perla Velasco & Yonathan Mtz. & Jorge Solís
# Editor(es): Andrea Barragan & Esneralda Pacheco & Naylea Castrellon & Daniel Esparza
# Version: 3.0.0 Marzo 2022
# Descripción:
#
#   Este archivo define el punto de ejecución del Publicador
#
#-------------------------------------------------------------------------
import random
from src.patient import Patient
from src.helpers.publicador import publish
import time

if __name__ == '__main__':
    print("Iniciando simulación del sistema SMAM...")
    older_patients = []
    total_patients = random.randint(1, 5)
    print(f"actualmente hay {total_patients} adultos mayores...")
    for _ in range(total_patients):
        older_patients.append(Patient())
    
    #older_patients.sort(key=lambda x: x.)
    #older_patients.sort()
    #older_patients_sorted = []
    #temp_i = 0
    #for patient in older_patients: 
        #temp_i = temp_i+1
        #patient.check_devices()  
        #patient_hrs = temp_i.patient.timer.hrs
        #print("num ",temp_i)
        #print("hrsss ",patient_hrs)
        #for index in range(temp_i, total_patients):    
            #if(older_patients[ patient_hrs] > older_patients[ patient_hrs]):    
                #temp = older_patients[ temp_i];    
                #older_patients[ temp_i] = older_patients[ index];    
                #older_patients[ index] = temp;

    print("Comenzando monitoreo de signos vitales...")
    print()
    for patient in older_patients:
            print("extrayendo signos vitales...")
            patient.check_devices()
            print()
            print("analizando signos vitales...")
            if patient.wearable.temperature < 34:
                print("anomalía detectada, notificando signos vitales...")
                publish('notifier', patient.to_json())
            print()
            if patient.accelerometer.ejeX == 0:
                print("caída detectada, notificando...")
                publish('caida', patient.to_json())
                print()
            print("alerta de medicina, notificando...")
            publish('temporizador', patient.to_json())
            print("actualizando expediente...")
            publish('record', patient.to_json())
            publish('monitorSVitales', patient.to_json())
            publish('monitorCaida', patient.to_json())
            publish('monitorMedicina', patient.to_json())
            time.sleep(1)

