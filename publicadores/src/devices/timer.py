##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: timer.py
# Capitulo: Estilo Publica-Suscribe
# Autor(es): Perla Velasco & Yonathan Mtz. & Jorge Solís
# Editor(es): Andrea Barragan & Esneralda Pacheco & Naylea Castrellon & Daniel Esparza
# Version: 3.0.0 Marzo 2022
# Descripción:
#
#   Esta clase define el publicador que enviará mensajes hacia el distribuidor de mensajes
#
#   A continuación se describen los métodos que se implementaron en esta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |       __init__()       |  - self: definición de   |  - constructor de la  |
#           |                        |    la instancia de la    |    clase              |
#           |                        |    clase                 |                       |
#           +------------------------+--------------------------+-----------------------+
#           |          run()         |  - self: definición de   |  - simula la          |
#           |                        |    la instancia de la    |    alarma que         |
#           |                        |    la instancia de la    |    determina el       |
#           |                        |                          |    momento en el que  |
#           |                        |                          |    se debe            |
#           |                        |                          |    administrar algún  |
#           |                        |                          |    medicamento a los  | 
#           |                        |                          |    adultos mayores    |
#           +------------------------+--------------------------+-----------------------+
#           |      medicine()        |  - self: definición la   |    selecciona una     |
#           |                        |    medicina aleatoria    |    medicina aleatoria |
#           |                        |    que debe tomar el     |    de un grupo de     |
#           |                        |           paciente       |    medicinas          |
#           |                        |                          |    y regresa su valor |
#           |                        |                          |    en String  | 
#           |                        |                          |                       |
#           +------------------------+--------------------------+-----------------------+
#           |         horas()        |  - self: definición la   |    selecciona una     |
#           |                        |    hora aleatoria en la  |    hora aleatoria     |
#           |                        |    que debe tomar        |    de un rango de     |
#           |                        |    medicina el paciente  |    números            |
#           |                        |                          |    y regresa su valor |
#           |                        |                          |                       | 
#           |                        |                          |                       |
#           +------------------------+--------------------------+-----------------------+
#           |         dosis()        |  - self: definición la   |    selecciona una     |
#           |                        |    medicina aleatoria    |    medicina aleatoria |
#           |                        |    que debe tomar el     |    de un grupo de     |
#           |                        |           paciente       |    medicinas          |
#           |                        |                          |    y regresa su valor |
#           |                        |                          |    en String  | 
#           |                        |                          |                       |
#           +------------------------+--------------------------+-----------------------+
#
#-------------------------------------------------------------------------
from faker import Faker
import random

class Timer:

    def __init__(self):
        fake = Faker()
        self.id = fake.numerify(text="%%######")

    def run(self):
        self.hrs = self.horas()
        self.med = self.medicine()
        self.dose = self.dosis()

    def medicine(self):
        self.medicine = random.choice(["Paracetamol", "Dipirona magnésica", "Dipirona hioscina", "Tramadol", 
            "Antidepresivo", "Aspirina", "Antiarritmico", "Diuretico"])
        self.medicinaAleatorioa = str(self.medicine)
        return(self.medicinaAleatorioa)

    def horas(self):
        self.time = random.randint(1,10)
        return (self.time)

    def dosis(self):
        self.dosis = random.choice(["10mg", "20ml", "30ml", "40mg"])
        self.doseAleatorio = str(self.dosis)
        return(self.doseAleatorio)